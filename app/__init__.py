from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from crack_captcha import Predictor
import logging 
import tensorflow as tf

app = Flask(__name__)
app.config.from_object('db_config')
app.config['PRESERVE_CONTEXT_ON_EXCEPTION'] = False
db = SQLAlchemy(app)
predictor = Predictor() #TODO: not with apache!
graph = tf.get_default_graph()

from app import models
