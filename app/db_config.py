import os
host = 'localhost'
passwd = ''
user = 'root'
dbname = 'rrknews'

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'mysql://%s@%s/%s' % (user, host, dbname)
if passwd != '':
	SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@%s/%s' % (user,passwd,host,dbname)
#'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
