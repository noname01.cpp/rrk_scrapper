from app import db

class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sabtNo = db.Column(db.String(11), index=True, unique=True, nullable=False)
    company_news = db.relationship('News', backref='company', lazy='dynamic')
    company_errors = db.relationship('Error', backref='company', lazy='dynamic')
    timestamp = db.Column(db.DateTime)

    def __repr__(self):
        return '<Company %r>' % (self.sabtNo)

class News(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    
    code = db.Column(db.String(16), nullable=False)
    is_old = db.Column(db.SmallInteger, nullable=False) #db.Boolean won't work with mysql!
    
    timestamp = db.Column(db.DateTime)
    
    newspaper_number = db.Column(db.Unicode(64))
    newspaper_city = db.Column(db.Unicode(20))
    newspaper_page_number = db.Column(db.Unicode(8))
    newspaper_date = db.Column(db.Unicode(20)) #TODO: change it to date?
    sabt_mail_number = db.Column(db.Unicode(64))
    sabt_mail_date = db.Column(db.Unicode(20)) #TODO: change it to date?
    title = db.Column(db.Unicode(500))
    newspaper_reference_number = db.Column(db.Unicode(64))
    body = db.Column(db.Unicode(400*20))
    

    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))
    __table_args__ = (db.UniqueConstraint('code', 'is_old', name='_code_is_old_uc'), db.Index('_code_is_old_idx', "code", "is_old"), )
    #__table_args__ = ()
    def __repr__(self):
        return '<News %r, %r>' % (self.code, self.is_old)

class Error(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code =  db.Column(db.String(16))
    status = db.Column(db.String(32))
    reason = db.Column(db.String(256))
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))

    def __repr__(self):
        return '<Error %r>' % (self.status)
