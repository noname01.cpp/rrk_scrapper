from flask import Flask, jsonify
import time
from web_scrapper import WebScrapper
import config
from app import predictor
from app import models
from app import db
from app import app
from app import graph
import logging
import datetime
import requests

@app.route('/test_rrk/')
def test_rrk():
    return requests.get('http://rrk.ir').content


@app.teardown_request
def teardown_request(exception):
    if exception:
        db.session.rollback()
        #db.session.remove()
    #db.session.remove() #TODO: not sure!

@app.route('/news_scrapper/api/search_sabt_no/<sabt_no>', methods=['POST'])
def search_sabt_no(sabt_no):
    #TODO: rollbacks!
    if type(sabt_no) is unicode:
        sabt_no = str(sabt_no)
    scrapper = WebScrapper(predictor=predictor)
    #TODO: only search for new (code,is_old)s?
    results = scrapper.search_sabt_no_sequential_full(sabt_no)
    if len(results) > 0:
        cur_company = models.Company.query.filter_by(sabtNo=sabt_no).first()
        if cur_company is None:
            cur_company = models.Company(sabtNo=sabt_no, timestamp=datetime.datetime.utcnow()) #TODO: utc time!
            db.session.add(cur_company)
            db.session.commit()
            cur_company = models.Company.query.filter_by(sabtNo=sabt_no).first()

        for result in results:
            if result.has_key('status'):
                if result['status'] == config.OK_CODE:
                    cur_news = models.News.query.filter_by(code=result['code'], is_old=result['is_old']).first()
                    if cur_news is None:
                        cur_news = models.News(code=result['code'],
                                       is_old=result['is_old'],
                                       body=result['news_text'],
                                       timestamp=datetime.datetime.utcnow(),
                                       newspaper_number=result['newspaper_number'],
                                       newspaper_city=result['newspaper_city'],
                                       newspaper_page_number=result['newspaper_page_number'],
                                       newspaper_date=result['newspaper_date'],
                                       sabt_mail_number=result['sabt_mail_number'],
                                       sabt_mail_date=result['sabt_mail_date'],
                                       title=result['news_title'],
                                       newspaper_reference_number=result['newspaper_reference_number'],
                                       company=cur_company)
                        db.session.add(cur_news)
                        db.session.commit() 
                        #cur_news = models.News.query.filter_by(code=result['code'], is_old=result['is_old']).first()
                    else:
                        if cur_news.company.id != cur_company.id:
                            logger.error('A code %s for two companies with ids %d, %d', result['code'], cur_news.company.id, cur_company.id)
                            cur_error = models.Error( code= result['code'] if result.has_key('code') else None,
                                                      status= config.DOUBLE_COMPANY_ERROR,
                                                      reason= 'A code %s for two companies with ids %d, %d' %  (result['code'], cur_news.company.id, cur_company.id),
                                                      company=cur_company)
                            db.session.add(cur_error)

                            cur_error = models.Error( code= result['code'] if result.has_key('code') else None,
                                                      status= config.DOUBLE_COMPANY_ERROR,
                                                      reason= 'A code %s for two companies with ids %d, %d' %  (result['code'], cur_news.company.id, cur_company.id),
                                                      company=cur_news.company)
                            db.session.add(cur_error)
                            db.session.commit()
                else: 
                    cur_error = models.Error( code=result['code'] if result.has_key('code') else None,
                                               status=result['status'] if result.has_key('status') else None,
                                               reason=result['reason'] if result.has_key('reason') else None,
                                               company=cur_company)
                    db.session.add(cur_error)
                    db.session.commit()
    return jsonify({'results': results})

if __name__ == '__main__':
    logging.basicConfig( level=logging.DEBUG,
                         format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                         datefmt='%m-%d %H:%M',
                         filename='news_scrapper.log',
                         filemode='a')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    #predictor = Predictor()
    app.run(debug=False)
