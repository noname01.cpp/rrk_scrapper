import sys
import logging
sys.path.insert(0, '/Users/amirrahimi/tempNew/news_agahi/')
sys.path.insert(0, '/Users/amirrahimi/tempNew/news_agahi/app/' )
logging.basicConfig( level=logging.WARNING,
                         format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                         datefmt='%m-%d %H:%M',
                         stream=sys.stderr)

from run import app as application
