import os
basedir = os.path.abspath(os.path.dirname(__file__))

DRIVER = 'phantomjs'

HOME_URL = 'http://www.rrk.ir/News/NewsList.aspx'

SABT_NO_ID = 'cphMain_txtSabtNationalID'
SABT_NO_LENGTH = 11

SABT_SEARCH_BTN_ID = 'cphMain_btnSearch'
SABT_BEFORE_1394_SELECTOR_ID = 'cphMain_ddlNewArchive'
LINK_ATTR_NEW = 'ShowNews'
LINK_ATTR_OLD = 'ShowOldNews'

SHOW_NEWS_URL = 'http://rrk.ir/News/ShowNews.aspx?Code='
SHOW_OLD_NEWS_URL = 'http://www.rrk.ir/News/ShowOldNews.aspx?Code='

NEXT_PAGE_ID = 'cphMain_rptPagingRec_btnNextPage'

CAPTCHA_ID = 'imgCaptcha'
CAPTCHA_TXT_ID = 'txtCaptcha'
CAPTCHA_BTN_ID = 'cphMain_btnCaptcha'

NEWS_IS_SHOWN_ID = 'cphMain_pnlShowNews'


DO_SLEEP = False
SLEEP_DURATION = 2

OK_CODE = 'OK'
ERROR_CODES = []
CODE_ERROR = 'CODE_ERROR'
ERROR_CODES.append(CODE_ERROR)

INPUT_ERROR = 'INPUT_ERROR'
ERROR_CODES.append(INPUT_ERROR)

HTML_ERROR = 'HTML_ERROR'
ERROR_CODES.append(HTML_ERROR)

CAPTCHA_ERROR = 'CAPTCHA_ERROR'
ERROR_CODES.append(CAPTCHA_ERROR)

DOUBLE_COMPANY_ERROR = 'DOUBLE_COMPANY_ERROR'
ERROR_CODES.append(DOUBLE_COMPANY_ERROR)

ADD_CAPTCHA_PREDICTOR = True
CAPTCHA_PRED_TRIES = 10
CAPTCHA_PRED_WEIGHT_FILE = os.path.join(basedir, 'mnist_train', 'weight.h5')

#NEWS DETAIL FIELDS
NEWSPAPER_NO_ID = 'cphMain_lblNewspaperNo'
NEWSPAPER_CITY_ID = 'cphMain_lblNewsPaperCityType'
NEWSPAPER_PAGE_NUMBER_ID = 'cphMain_lblPageNumber'
NEWSPAPER_DATE_ID = 'cphMain_lblNewsPaperDate'
SABT_MAIL_NUMBER_ID = 'cphMain_lblIndikatorNumber'
SABT_MAIL_DATE_ID = 'cphMain_lblNewsDate'
NEWS_TITLE_ID = 'cphMain_lblNewsTitle'
NEWSPAPER_REFERENCE_NUMBER_ID = 'cphMain_lblReferenceNumber'
NEWS_TEXT_DETAIL_CLASS_NAME = 'Jus'
