import numpy as np
import cv2
import glob
import sys, os
import logging
import config
from save_captchas import url_to_image
#import tensorflow as tf
#import __builtin__

def find_exterma(data, show = False):
    a = np.diff(np.sign(np.diff(data))).nonzero()[0] + 1 # local min+max
    b = (np.diff(np.sign(np.diff(data))) > 0).nonzero()[0] + 1 # local min
    c = (np.diff(np.sign(np.diff(data))) < 0).nonzero()[0] + 1 # local max
    x = np.arange(len(data))
    if show:
        import pylab as pl
        pl.plot(x,data)
        pl.plot(x[b], data[b], "o", label="min")
        pl.plot(x[c], data[c], "o", label="max")
        pl.legend()
        pl.show()
    return b,c

def find_splits(sum_xy, tr):
    min_idxes, max_idxes = find_exterma(sum_xy)
    vals = np.append(min_idxes, max_idxes)
    tmp  = np.array([0, len(sum_xy)-1])
    vals = np.append(vals, tmp)
    vals = np.sort(vals)
    up = True
    splits = []
    for i, val in enumerate(vals):
        if up:
            if sum_xy[val] >= tr:
                splits.append(vals[i-1])
                up = False
        else:
            if sum_xy[val] < tr:
                up = True
                splits.append(val)
    return splits

def split_vertical(img, tr=25000, med_size=5):
    med_cap = cv2.medianBlur(img, med_size)
    grad_y = cv2.Sobel(med_cap, cv2.CV_64F,0,1, ksize=5)
    sum_y = np.sum(np.abs(grad_y), axis=0).mean(axis=-1)
    return find_splits(sum_y, tr)

def split_horizontal(img, tr=10000, med_size=5):
    med_cap = cv2.medianBlur(img, med_size)
    grad_x = cv2.Sobel(med_cap, cv2.CV_64F,1,0, ksize=5)
    sum_x = np.sum(np.abs(grad_x), axis=1).mean(axis=-1)
    splits = find_splits(sum_x, tr)
    return splits


def segment_image(img, tr_vert=25000, tr_hor=10000, med_size = 5):
    splits = split_vertical(img, tr_vert, med_size)
    last_split = False
    segments = []
    for k in range(0, len(splits), 2):
            save_split = img[:,splits[k]:splits[k+1], :]
            if save_split.shape[1] >= 13:
                if last_split and k-2 >= 0:
                    save_split = img[:,splits[k-2]:splits[k+1], :]
                y_split = split_horizontal(save_split, tr_hor, med_size)
                t = max(0, y_split[0] - 2)
                b = min(save_split.shape[0]-1, y_split[-1] + 2)
                save_split = save_split[t:b,:,:]
                seg = cv2.medianBlur(save_split, med_size)
                if seg.shape[0] >= seg.shape[1]:
                    if seg.shape[0] > 24:
                        scale = 24.0 / seg.shape[0]
                        seg = cv2.resize(seg, (0,0), fx=scale, fy=scale)
                else:
                    if seg.shape[1] > 24:
                        scale = 24.0 / seg.shape[1]
                        seg = cv2.resize(seg, (0,0), fx=scale, fy=scale)
                seg_gray = 255 - seg.mean(axis=-1)
                seg_gray = seg_gray/seg_gray.max()
                x_pad = max(28 - seg_gray.shape[1], 0)
                y_pad = max(28 - seg_gray.shape[0], 0)
                seg_gray = np.pad( seg_gray, ((y_pad/2, y_pad - y_pad/2), (x_pad/2, x_pad - x_pad/2)), 'constant', constant_values=0)
                segments.append(seg_gray*255)
                last_split = False
            else:
                last_split = True
    return segments

class Predictor:
    def __init__(self, logger = None):
        self.logger = logger or logging.getLogger(__name__)
        self.logger.debug('__init__')
        import mnist_train.train as train
        self.model = train.get_model()
        self.model.load_weights(config.CAPTCHA_PRED_WEIGHT_FILE)
        #global graph
        #__builtin__.graph = tf.get_default_graph()
        self.logger.info('model created and weights loaded')

    def predict_url(self, url):
        #global graph
        from app import graph
        self.logger.debug('In predict_url')
        img = url_to_image(url)
        self.logger.info('Segmenting image by url %s', url)
        segments = segment_image(img)

        result = ''
        for segment in segments:
            to_pred = segment.reshape(1,28,28,1).astype('float32')/255
            #global graph
            with graph.as_default():
                result = result + str( np.argmax(self.model.predict(to_pred) ) )

        return result

def main2():
    img_list = glob.glob( sys.argv[1] + '*.png' )
    for j, image_file in enumerate(img_list):
        print image_file
        img = cv2.imread(image_file)
        segments = segment_image(img)
        for k,save_split in enumerate(segments):
            save_split_name = sys.argv[2] + '%05d_%d.png' % (j, k)
            cv2.imwrite(save_split_name, save_split)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: %s <captcha_folder> <output_folder>' % sys.argv[0]
        exit()
    main2()

    
