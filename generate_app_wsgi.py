import sys, os
basedir = os.path.abspath(os.path.dirname(__file__))
activate_this  = os.path.abspath( os.path.join( basedir, os.pardir, 'bin', 'activate_this.py') )
content = """
activate_this = '%s'
execfile(activate_this, dict(__file__=activate_this))

import sys
import logging
sys.path.insert(0, '%s/')
sys.path.insert(0, '%s/app/' )""" % (activate_this, basedir, basedir)

contin = """
logging.basicConfig( level=logging.WARNING,
                         format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                         datefmt='%m-%d %H:%M',
                         stream=sys.stderr)

from run import app as application
""" 

print content
print contin
