import os, getpass, grp, pwd
from os.path import expanduser


user = getpass.getuser()
gid = pwd.getpwnam(user).pw_gid
group = grp.getgrgid(gid).gr_name
home = expanduser("~")
basedir = os.path.abspath(os.path.dirname(__file__))

if not os.path.exists( os.path.join( home, '.logs') ):
        os.makedirs( os.path.join( home, '.logs') )

content = """
<VirtualHost *>
  <Directory %s>
    WSGIProcessGroup app
    WSGIApplicationGroup %s
    Require all granted
  </Directory>
  WSGIScriptAlias /app %s
  WSGIDaemonProcess app user=%s group=%s threads=5
  ErrorLog %s
  LogLevel warn
  CustomLog %s combined
</VirtualHost>
""" % ( 
    os.path.join(basedir, 'web'),
    '%{GLOBAL}',
    os.path.join(basedir, 'web', 'app.wsgi'),
    user, group,
    os.path.join(home, '.logs', 'apache_error.log') ,
    os.path.join(home, '.logs', 'apache_access.log')
    )

print content
