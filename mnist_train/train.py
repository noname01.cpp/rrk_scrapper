import numpy as np
from keras.datasets import mnist
#import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten, Reshape
from keras.optimizers import SGD, RMSprop
from keras.utils import np_utils
from keras.regularizers import l2
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, AveragePooling2D
from keras.callbacks import EarlyStopping
from keras.preprocessing.image import ImageDataGenerator
from keras.layers.normalization import BatchNormalization
from keras.callbacks import ModelCheckpoint
#import tensorflow as tf
#from PIL import Image

#graph = None

def load_data():
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    X_train = X_train.reshape(X_train.shape[0],  28, 28, 1).astype('float32') / 255
    X_test = X_test.reshape(X_test.shape[0], 28, 28, 1).astype('float32') / 255
    Y_train = np_utils.to_categorical(y_train, 10)
    Y_test = np_utils.to_categorical(y_test, 10)
    return X_train, Y_train, X_test, Y_test


def get_model():
    model = Sequential()

    model.add(Convolution2D(8, 2, 2, border_mode='same', input_shape = ( 28, 28, 1)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Convolution2D(16, 2, 2, border_mode='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Convolution2D(32, 2, 2, border_mode='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))


    model.add(Dropout(0.5))

   # model.add(Convolution2D(128, 1, 1, border_mode='valid'))

    model.add(Flatten())
    model.add(Dense(256))
    model.add(Activation("relu"))
    model.add(Dense(10))
    model.add(Activation('softmax'))

    #graph = tf.get_default_graph()
    return model

def train(model, X_train, Y_train, X_test, Y_test, nb_epoch=8):
    checkpointer = ModelCheckpoint(filepath="weights.{epoch:02d}-{val_loss:.3f}.hdf5", verbose=1, save_best_only=True)

    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    model.fit(X_train, Y_train, batch_size=32, nb_epoch=nb_epoch,
                   verbose=1, validation_data=(X_test, Y_test), callbacks=[checkpointer])
    model.save_weights('learned_model.h5')
    score = model.evaluate(X_test, Y_test, verbose=0)

    print("Test classification rate: ", score[1])

if __name__ == '__main__':
   X_train, Y_train, X_test, Y_test = load_data() 
   model = get_model()
   train(model, X_train, Y_train, X_test, Y_test, nb_epoch=20)
