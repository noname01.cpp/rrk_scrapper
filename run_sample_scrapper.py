import logging
import web_scrapper
import config
import sys

def main():
    logging.basicConfig( level=logging.DEBUG,
                         format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                         datefmt='%m-%d %H:%M',
                         filename='myapp.log',
                         filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    logging.info("Program started")

    scrapper = web_scrapper.WebScrapper()

    sabt_no = '10240139019'
    if len(sys.argv) == 2:
        sabt_no = str(sys.argv[1] )
    print sabt_no
    results = scrapper.search_sabt_no_sequential_full(sabt_no)
    for result in results:
        if result.has_key('status'):
            print '*'*20
            if result['status'] == config.OK_CODE:
                
                print 'Result for code %s is:' % result['code']
                #print result['text']
                if result.has_key('news_title'):
                    print result['news_title']
                else:
                    print result['text']
                print '\n\n'
            else:
                print 'Result for code %s has errors:' % result['code']
                print 'Reason: ', result['reason']
                print '\n\n'
                
            print '*'*20
        else:
            print 'Bad result!', result
    
    logging.info("Done!")

if __name__ == '__main__':
    main()
