import mechanize
import urllib
import numpy as np
import cv2

import bs4 as BeautifulSoup
import sys, os

url = 'http://rrk.ir/News/ShowNews.aspx?Code=12152064'

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
 
    # return the image
    return image

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: %s <num> <output-folder>' % sys.argv[0]
        exit()
    num = int(sys.argv[1])
    out_folder = sys.argv[2]

    for i in range(num):
        br = mechanize.Browser()
        br.open(url)
        response = br.response()
        html = response.read()
        soup = BeautifulSoup.BeautifulSoup(html, 'html.parser')
        image_tags = soup.findAll('img')
        img_link = 'http://rrk.ir/' + image_tags[1]['src'][3:]
        img = url_to_image(img_link)
        image_name = '%05d.png' % i
        print image_name
        cv2.imwrite( out_folder + image_name, img)
