import numpy as np
from selenium import webdriver
import bs4 as BeautifulSoup
import config
from selenium.common.exceptions import NoSuchElementException
import logging
import time
from selenium.webdriver.support.ui import Select
from crack_captcha import Predictor

class WebScrapper:

    def __init__(self, logger=None, predictor = None):
        self.logger = logger or logging.getLogger(__name__)
        self.logger.debug('__init__')
        self.driver = None
        if predictor is None:
            self.predictor = Predictor()
        else:
            self.predictor = predictor
        if config.DRIVER == 'phantomjs':
            self.driver = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true', '--ssl-protocol=any', '--web-security=false'])
            self.driver.set_window_size(1024, 768)
        else:
            self.driver = webdriver.Firefox()
        self.logger.info('Driver %s created', self.driver.name)

    def do_search_and_get_results(self, is_old = False):
        self.logger.debug('In do_search_and_get_results')
        driver = self.driver
        LINK_ATTR = config.LINK_ATTR_NEW
        if is_old:
            LINK_ATTR = config.LINK_ATTR_OLD
        try:
            driver.find_element_by_id(config.SABT_SEARCH_BTN_ID).click()
            if config.DO_SLEEP:
                time.sleep(config.SLEEP_DURATION)
            self.logger.debug('Search button clicked')
        except NoSuchElementException:
            self.logger.error('SABT_NO_ID and SABT_SEARCH_BTN_ID not found')
            return []
        all_codes = []
        while True:
            html = driver.page_source
            soup = BeautifulSoup.BeautifulSoup(html, 'html.parser')
            link_tags = soup.findAll('a')
            codes = [ str(link['href'].split('=')[-1]) for link in link_tags if link.attrs.has_key('href') and link.attrs['href'].find(LINK_ATTR) > 0 ]
            codes = codes[::2]
            all_codes.extend(codes)
            self.logger.info('%d codes found', len(codes))

            try:
                driver.find_element_by_id(config.NEXT_PAGE_ID).click()
                if config.DO_SLEEP:
                    time.sleep(config.SLEEP_DURATION)
            except NoSuchElementException:
                self.logger.info('No next page')
                break
            self.logger.info('Going to next page')
        return all_codes

    def search_sabt_no(self, sabt_no):
        self.logger.debug('In search_sabt_no')
        if type(sabt_no) is not str:
            self.logger.error('Sabt_no is not a string')
            return [], []
        elif not sabt_no.isalnum() or len(sabt_no) != config.SABT_NO_LENGTH:
            self.logger.error('Sabt_no is not a valid numeric string')
            return [], []
        else:
            self.logger.info('Searching for sabt_no: %s', sabt_no)
            driver = self.driver
            driver.get(config.HOME_URL)
            if config.DO_SLEEP:
                time.sleep(config.SLEEP_DURATION)
            self.logger.debug('Home url %s retrieved', config.HOME_URL)
            try:
                sabt_input = driver.find_element_by_id(config.SABT_NO_ID)
                sabt_input.send_keys(sabt_no)
            except NoSuchElementException:
                self.logger.error('SABT_NO_ID not found')
                return [], []
            codes = self.do_search_and_get_results(is_old = False) 
            self.logger.info('Searching for before 1394 codes')
            try:
                select = Select(driver.find_element_by_id(config.SABT_BEFORE_1394_SELECTOR_ID))
                select.select_by_value('2')
            except NoSuchElementException:
                self.logger.error('Before 1394 select not found!')
                return codes, []
            old_codes = self.do_search_and_get_results(is_old = True)
            return codes, old_codes

    def get_single_captcha_src(self, code, is_old = False):
        self.logger.debug('In get_single_captcha_src')
        self.logger.info('Processing code %s', code)
        if type(code) is not str:
            self.logger.error('Code %s is not str', str(code))
            return None
        url = config.SHOW_NEWS_URL + code
        if is_old:
            url = config.SHOW_OLD_NEWS_URL + code

        driver = self.driver
        driver.get(url)
        self.logger.info('Url for %s retrieved', url)
        if config.DO_SLEEP:
            time.sleep(config.SLEEP_DURATION)
        src = None
        try:
            captcha_tag = driver.find_element_by_id(config.CAPTCHA_ID)
            self.logger.debug('Captcha element found')
            src = captcha_tag.get_attribute('src')
        except NoSuchElementException:
            self.logger.error('Captcha element for code %s not found', code)

        return src

    def add_element_if_found(self, element, key_name, result, is_ID = True):
        driver = self.driver
        try:
            div = None
            if is_ID:
                div = driver.find_element_by_id(element)
            else:
                div = driver.find_element_by_class_name(element)
            result[key_name] = div.text
        except NoSuchElementException:
            result[key_name] = None

    def try_prediction(self, code,  src):
        
        result = {}
        new_src = src
        driver = self.driver

        for i in range(config.CAPTCHA_PRED_TRIES):
            txt = self.predictor.predict_url( new_src )
            try:
                captcha_txt = driver.find_element_by_id(config.CAPTCHA_TXT_ID)
                captcha_txt.send_keys(txt)
                driver.find_element_by_id(config.CAPTCHA_BTN_ID).click()
                self.logger.info('Captcha button clicked for code %s', code)
            except:
                self.logger.error('captcha text/button id not found')
                result['status'] = config.HTML_ERROR
                result['reason'] = 'captcha text/button id in html not found!'
                return result
            if config.DO_SLEEP:
                time.sleep(config.SLEEP_DURATION)
            try:
                result_div = driver.find_element_by_id(config.NEWS_IS_SHOWN_ID)
                self.logger.info('News details found for code %s', code)
                result['status'] = config.OK_CODE
                result['text'] = result_div.text
                result['page_source'] = driver.page_source
                # NEWSPAPER NO
                self.add_element_if_found( config.NEWSPAPER_NO_ID, 'newspaper_number', result, is_ID = True)
                self.add_element_if_found( config.NEWSPAPER_CITY_ID, 'newspaper_city', result, is_ID = True)
                self.add_element_if_found( config.NEWSPAPER_PAGE_NUMBER_ID, 'newspaper_page_number', result, is_ID = True)
                self.add_element_if_found( config.NEWSPAPER_DATE_ID, 'newspaper_date', result, is_ID = True)
                self.add_element_if_found( config.SABT_MAIL_NUMBER_ID, 'sabt_mail_number', result, is_ID = True)
                self.add_element_if_found( config.SABT_MAIL_DATE_ID, 'sabt_mail_date', result, is_ID = True)
                self.add_element_if_found( config.NEWS_TITLE_ID, 'news_title', result, is_ID = True)
                self.add_element_if_found( config.NEWSPAPER_REFERENCE_NUMBER_ID, 'newspaper_reference_number', result, is_ID = True)
                self.add_element_if_found( config.NEWS_TEXT_DETAIL_CLASS_NAME, 'news_text', result, is_ID = False)
                return result
            except NoSuchElementException:
                self.logger.info('Captcha %s for code %s seems to be wrong', txt, code)
                try:
                    captcha_tag = driver.find_element_by_id(config.CAPTCHA_ID)
                    self.logger.debug('Captcha element found')
                    new_src = captcha_tag.get_attribute('src')
                    self.logger.info('Retry #%d ...', i+1)
                except NoSuchElementException:
                    self.logger.error('Captcha element for code %s not found', code)
                    result['status'] = config.HTML_ERROR
                    result['reason'] = 'Captcha txt/id for code %s not found after captcha txt was wrong!' % code
                    return result

        result['reason'] = 'captcha cannot be guessed for code %s, src %s after retries!' % ( code, new_src)
        result['status'] = config.CAPTCHA_ERROR
        result['src'] = new_src
        return result

    def get_news_text(self, codes, is_old = False):
        results = []
        for code in codes:
            src = self.get_single_captcha_src( code, is_old )
            if src is not None:
                result = self.try_prediction( code,  src)
                result['code'] = code
                result['is_old'] = is_old
                results.append(result)
            else:
                result = {}
                result['status'] = config.HTML_ERROR
                result['reason'] = 'src for code %s is None!' % str(code)
                result['code'] = code
                result['is_old'] = is_old
                results.append(result)
        return results


    def search_sabt_no_sequential_full(self, sabt_no):
        self.logger.debug('In sequential_search')
        codes, old_codes = self.search_sabt_no(sabt_no)
        results = self.get_news_text(codes, False)
        results.extend( self.get_news_text( old_codes, True ) )
        return results
       
    def __del__(self):
        self.logger.debug('In destructor')
        self.driver.quit()
